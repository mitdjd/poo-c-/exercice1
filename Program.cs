﻿using System;

namespace excercice1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Console.WriteLine("Hello World!");
            firstQst();
            secondQst();
            System.Console.WriteLine(thirdQst());
            fourthSqt();
        }

        /// <summary>
        /// First Question
        /// </summary>
        public static void firstQst() {
            // TODO 
            System.Console.WriteLine("Question 1 : ");
            System.Console.WriteLine("Please enter a number between 1 and 10");
            int number = int.Parse(Console.ReadLine());
            System.Console.WriteLine(number <= 10 && number >= 1 ? "Valid": "Invalide");
            Console.ReadKey();

        }


        /// <summary>
        /// Second Question
        /// </summary>
        public static void secondQst() {
            // TODO 
            System.Console.WriteLine("Question 2 : ");
            System.Console.WriteLine("Please enter the first number");
            float firstNumber = float.Parse(Console.ReadLine());
            System.Console.WriteLine("Please enter the second number");
            float secondNumber = float.Parse(Console.ReadLine());
            //
            System.Console.WriteLine(firstNumber > secondNumber ? firstNumber : secondNumber);
            Console.ReadKey();
        }

        /// <summary>
        /// Third SQt
        /// </summary>
        /// <returns></returns>
        public static string thirdQst() {
            // TODO 
            System.Console.WriteLine("Question 3 : ");
            System.Console.WriteLine("Please enter the height of the image");
            float height = float.Parse(Console.ReadLine());
            System.Console.WriteLine("Please enter the width of the image");
            float width = float.Parse(Console.ReadLine());
            //
            return (height > width ? ImageOrientation.Portrait : ImageOrientation.Landscape).ToString();
        }

        /// <summary>
        /// the last Qst
        /// </summary>
        public static void fourthSqt() {
            // TODO 
            System.Console.WriteLine("Question 4 : ");
            System.Console.WriteLine("Please enter the speed limit");
            int limit = int.Parse(Console.ReadLine());
            System.Console.WriteLine("Please enter the car speed");
            int carSpeed = int.Parse(Console.ReadLine());
            //
            if(carSpeed <= limit ) {
                System.Console.WriteLine("OK");
            } else {
                var points = calculatePoints(limit , carSpeed);
                if(points > 12)
                    System.Console.WriteLine("License Suspended");
                else 
                    System.Console.WriteLine(points + " points");
            }
        }

        /// <summary>
        /// calculate the point of diff between limit speed and car speed
        /// </summary>
        /// <param name="limit">limit speed</param>
        /// <param name="carSpeed">car speed</param>
        /// <returns></returns>
        private static int calculatePoints(int limit, int carSpeed) {
            // TODO 
            int diff = carSpeed - limit;
            int points = diff / 5;
            //
            return points;
        }

    }

    enum ImageOrientation
    {
        Landscape = 0,
        Portrait = 1
    }
}
